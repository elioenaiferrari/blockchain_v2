const sha256 = require('crypto-js/sha256');
const bcrypt = require('bcryptjs');

class Blockchain {
  constructor() {
    this.blocks = [];
    this.isValid = true;
  }

  lastBlock() {
    return this.blocks[this.blocks.length - 1];
  }

  addBlock({ data }) {
    const lastBlock = this.lastBlock();

    const index = lastBlock?.index + 1 || 0;
    const timestamp = new Date();
    const previousHash = lastBlock?.hash || null;
    const hash = sha256(
      `${index}${previousHash}${JSON.stringify(data)}${timestamp}`
    );

    this.blocks.push({ index, timestamp, previousHash, hash: `${hash}`, data });

    return this;
  }

  validate() {
    this.blocks.map((block, index, array) => {
      const nextBlock = array[index + 1];

      if (index < array.length - 1 && nextBlock.previousHash !== block.hash) {
        this.isValid = false;
      }
    });

    return this;
  }
}

module.exports = Blockchain;
