const cp = require('child_process');
const faker = require('faker');
const Blockchain = require('./entities/blockchain');
const { createWriteStream } = require('fs');

const blockchain = new Blockchain(`${__dirname}/blockchain.json`);

for (let index = 0; index <= 1000; index++) {
  console.log(`${index} blocks inserted`);
  blockchain.addBlock({
    data: {
      name: faker.name.firstName(),
      product: faker.commerce.product(),
      price: faker.commerce.price(),
    },
  });
}

blockchain.validate();

const stream = createWriteStream(`${__dirname}/data.json`, { flags: 'w+' });
stream.write(JSON.stringify(blockchain));
