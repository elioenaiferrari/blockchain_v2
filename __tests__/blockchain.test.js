const { describe, it, expect } = require('@jest/globals');
const Blockchain = require('../src/entities/blockchain');

describe('blockchain', () => {
  it('#create blockchain', () => {
    const blockchain = new Blockchain();

    expect(blockchain.blocks.length).toBe(0);
  });

  it('#add block to blockchain', () => {
    const blockchain = new Blockchain();

    blockchain.addBlock({
      data: { hello: 'my little friend' },
    });

    expect(blockchain.blocks.length).toBe(1);
  });

  it('#add two blocks to blockchain using builder pattern', () => {
    const blockchain = new Blockchain();

    blockchain
      .addBlock({ data: { hello: 'my little friend' } })
      .addBlock({ hash: '123124', data: { hello: 'world' } });

    expect(blockchain.blocks.length).toBe(2);
  });

  it('#set a valid index to block', () => {
    const blockchain = new Blockchain();

    blockchain
      .addBlock({
        data: { hello: 'my little friend' },
      })
      .addBlock({
        data: { hello: 'darkness' },
      });

    expect(blockchain.blocks.length).toBe(2);
    expect(blockchain.blocks[0].index).toBe(0);
    expect(blockchain.blocks[1].index).toBe(1);
  });

  it('#set a valid previous hash to block', () => {
    const blockchain = new Blockchain();

    blockchain
      .addBlock({ data: { hello: 'my little friend' } })
      .addBlock({ data: { hello: 'darkness' } });

    expect(blockchain.blocks.length).toBe(2);
    expect(blockchain.blocks[0].index).toBe(0);
    expect(blockchain.blocks[1].index).toBe(1);
  });

  it('#validate blockchain integrity if is valid', () => {
    const blockchain = new Blockchain();

    blockchain
      .addBlock({ data: { hello: 'my little friend' } })
      .addBlock({ data: { hello: 'darkness' } })
      .addBlock({ data: { hello: 'darkness' } })
      .addBlock({ data: { hello: 'darkness' } })
      .addBlock({ data: { hello: 'darkness' } })
      .addBlock({ data: { hello: 'darkness' } })
      .addBlock({ data: { hello: 'darkness' } });

    const { isValid } = blockchain.validate();

    expect(isValid).toBe(true);
    expect(blockchain.blocks.length).toBe(7);
  });

  it('#validate blockchain integrity if is invalid', () => {
    const blockchain = new Blockchain();

    blockchain
      .addBlock({
        data: { hello: 'my little friend' },
      })
      .addBlock({
        data: { hello: 'darkness' },
      })
      .addBlock({
        data: { hello: 'darkness' },
      });
    blockchain.blocks[1] = blockchain.blocks[0];

    const { isValid } = blockchain.validate();

    expect(isValid).toBe(false);
  });
});
